package com.crazy.shell.dao.mapper.mybatis;

import com.crazy.shell.common.persistent.dao.mapper.Mapper;
import com.crazy.shell.dao.entity.ComService;
import com.crazy.shell.dao.entity.Users;

public interface UsersMapper extends Mapper<Users> {

}