package com.crazy.shell.dao.entity;

import com.crazy.shell.common.persistent.dao.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "shell_company")
public class Company extends BaseEntity implements Serializable {


    @Column(name="USER_ID")
    private Integer userId;

    @Column(name="NAME")
    private String name;



    private static final long serialVersionUID = 1L;



    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }


    @Override
    public String toString() {
        return "Company{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                "} " + super.toString();
    }
}