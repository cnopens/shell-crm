package com.crazy.shell.dao.bean;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
public class BaseEnum {
    public static enum EnableEnum{
        Delete("删除",0),
        Enable("正常",1);
        

        private final String value;
        private final int key;

        EnableEnum(String value, int key) {
            this.value = value;
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public int getKey() {
            return key;
        }
    }
}
