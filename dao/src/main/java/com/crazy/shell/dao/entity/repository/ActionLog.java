package com.crazy.shell.dao.entity.repository;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Map;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/19.
 */
@Document
public class ActionLog  {

    /**
     * 主键ID
     */
    @Id
    private String id;

    /**
     * 请求地址
     */
    private String address;
    /**
     * 请求方法
     */
    private String method;
    /**
     * 请求参数
     */
    private Map<String,String> param;
    /**
     * 返回类型
     */
    private String reType;
    /**
     * 返回内容
     */
    private String reVal;
    /**
     * 操作人
     */
    private Integer userId;
    /**
     * 操作时间
     */
    private Date createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }


    public Map<String, String> getParam() {
        return param;
    }

    public void setParam(Map<String, String> param) {
        this.param = param;
    }

    public String getReType() {
        return reType;
    }

    public void setReType(String reType) {
        this.reType = reType;
    }

    public String getReVal() {
        return reVal;
    }

    public void setReVal(String reVal) {
        this.reVal = reVal;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    @Override
    public String toString() {
        return "ActionLog{" +
                "address='" + address + '\'' +
                ", method='" + method + '\'' +
                ", param=" + param +
                ", reType='" + reType + '\'' +
                ", reVal='" + reVal + '\'' +
                ", userId=" + userId +
                ", createTime=" + createTime +
                '}';
    }
}
