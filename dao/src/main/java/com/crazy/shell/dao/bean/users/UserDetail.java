package com.crazy.shell.dao.bean.users;

import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.dao.entity.Users;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/8/5.
 */
public class UserDetail {
    /**
     * usersId
     */
    private int uid;
    /**
     * companyId
     */
    private int cid;
    /**
     * 账号
     */
    private String userName;
    /**
     * 企业名
     */
    private String companyName;


    private Users users;

    private Company company;


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getUserName() {
        String userName = this.userName;
        if(this.users!=null){
            userName = users.getUsername();
        }
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCompanyName() {
        String name = this.companyName;
        if(this.company!=null){
            name = this.company.getName();
        }
        return name;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "UserDetail{" +
                "uid=" + uid +
                ", cid=" + cid +
                ", userName='" + userName + '\'' +
                ", companyName='" + companyName + '\'' +
                '}';
    }
}
