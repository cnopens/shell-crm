package com.crazy.shell.dao.bean;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/11.
 */
public class UserEnum {
    public static enum RoleEnum{
        Manager("管理员",1),
        Register("注册用户",0);


        private final String value;
        private final int key;

        RoleEnum(String value, int key) {
            this.value = value;
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public int getKey() {
            return key;
        }
    }
}
