package comn.crazy.shell.web;

import org.apache.catalina.Context;
import org.apache.catalina.Engine;
import org.apache.catalina.Host;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.AprLifecycleListener;
import org.apache.catalina.core.StandardServer;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;
import java.io.File;

public class CloseRunner {

	public static void main(String[] args) throws Exception {


		Tomcat tomcat = new Tomcat();

		System.setProperty("resources.config.path", "/Users/creazier.huang/Desktop/config/shell");

		String baseDir = new File("").getAbsolutePath();

		tomcat.setBaseDir(baseDir + "/src/test/resources/tomcat-home");

		tomcat.enableNaming();

		tomcat.addWebapp("/website", baseDir + "/website/src/main/webapp");

		tomcat.setPort(8081);
		tomcat.start();
		tomcat.getServer().await();
	}

}
