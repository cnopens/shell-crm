package com.crazy.shell.website.controller.comSer;

import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;
import com.crazy.shell.api.comService.ComServiceProvider;
import com.crazy.shell.api.company.CompanyProvider;
import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.entity.ComService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/29.
 */
@Controller
@RequestMapping("/comser")
public class ComSerController {

    @Autowired
    private ComServiceProvider comSerProvider;
    @Autowired
    private CompanyProvider companyProvider;

    @RequestMapping("toList.htm")
    public String toList(){
        return "/guest/list";
    }

    @RequestMapping("list.ajax")
    public ModelAndView list(HttpServletRequest request, HttpServletResponse response){
        ActionResult result = new ActionResult();
        ComService config = new ComService();

        SSOToken st = SSOHelper.getToken(request);
        comSerProvider.findByObj(config);
//        result.attach("dataRows",list);
        return result.getJsonModelView();
    }

}
