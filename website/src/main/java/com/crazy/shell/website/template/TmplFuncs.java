package com.crazy.shell.website.template;

import javax.servlet.http.HttpServletRequest;

public interface TmplFuncs {
	String toHtml(HttpServletRequest request, String templateName);
}
