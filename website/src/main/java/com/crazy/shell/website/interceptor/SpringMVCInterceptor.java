package com.crazy.shell.website.interceptor;

import com.alibaba.fastjson.JSON;
import com.crazy.shell.dao.entity.repository.ActionLog;
import com.crazy.shell.kafka.ShellProducer;
import com.crazy.shell.website.common.Constant;
import com.crazy.shell.website.util.RequestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 项目名称：shell-crm
 * 创建人: crazy.huang
 * 创建时间：2016/1/20.
 */

public class SpringMVCInterceptor extends HandlerInterceptorAdapter implements InitializingBean,Constant {

    private final Logger logger = LoggerFactory.getLogger(SpringMVCInterceptor.class);

    @Value("${web.staticPath}")
    private String staticPath;
    @Value("${web.uploadPath}")
    private String uploadPath;

    @Resource
    private ShellProducer producer;

    /**
     * 开始时间
     */
    private Long startStamp;

    /**
     * 结束时间
     */
    private Long endStamp;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("start request >> " + request.getRequestURI() + " by " + request.getHeader("user-agent"));
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        this.startStamp = System.currentTimeMillis();

        request.setAttribute("uri", request.getRequestURI());
        request.setAttribute("staticPath",staticPath);
        request.setAttribute("uploadPath",uploadPath);
        request.setAttribute("version","1.0.0");
//        request.setAttribute("ctx", contextPath);
//        request.setAttribute("uri", request.getRequestURI());
//        request.setAttribute("staticPath", staticPath);
//        request.setAttribute("uploadPath", uploadPath);
//        request.setAttribute("version", "1.0.2");
//        request.setAttribute("textFuncs", textFuncs);
//        request.setAttribute("sessionId", request.getSession().getId());
//        request.setAttribute("mainUrl", mainUrl);
        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        this.endStamp = System.currentTimeMillis();
        HandlerMethod handlerMethod = (HandlerMethod) handler;

        logger.info("方法名称 : {},开始时间 : {},结束时间 : {},总共消耗 : {}ms"+handlerMethod.getMethod().getName() ,this.startStamp,this.endStamp,(this.endStamp - this.startStamp));


//        producer.producer();
        ActionLog log = new ActionLog();
        log.setMethod(handlerMethod.getMethod().getName());
        log.setAddress(RequestUtil.getIpAddr(request));
        log.setCreateTime(new Date());
//        log.setParam();
        producer.producer(JSON.toJSONString(log));

        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }

    public void afterPropertiesSet() throws Exception {

    }
}
