package com.crazy.shell.server.users;

import com.crazy.shell.api.base.BaseProvider;
import com.crazy.shell.api.users.UsersProvider;
import com.crazy.shell.common.persistent.service.BaseService;
import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.bean.BaseEnum;
import com.crazy.shell.dao.bean.users.UserDetail;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.dao.entity.Users;
import com.crazy.shell.server.base.BaseProviderImpl;
import com.crazy.shell.service.users.UsersService;
import com.crazy.shell.service.users.UsersServiceImpl;
import com.sun.corba.se.spi.orbutil.fsm.Action;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
@com.alibaba.dubbo.config.annotation.Service(cluster="failover",retries=2)
public class UserProviderImpl extends BaseProviderImpl<Users> implements UsersProvider {

    private static Logger logger = LoggerFactory.getLogger(UserProviderImpl.class);

    @Resource
    private UsersService usersService;

    @Override
    public BaseService<Users> getBaseService() {
        return usersService;
    }

    @Override
    public ActionResult register(Company company, Users users) {
        ActionResult result = new ActionResult();

        int count = usersService.countOrObj(users);
        if(count > 0 ){
            result.setMessages("该用户已被注册");
            return result;
        }

        result = usersService.addTran(company,users);

        return result;
    }

    @Override
    public ActionResult getByObj(Users users) {
        ActionResult result = new ActionResult();

        if(StringUtils.isEmpty(users.getUsername())){
            result.setMessages("用户名不能为空");
            return result;
        }

        if(StringUtils.isEmpty(users.getPassword())){
            result.setMessages("密码不能为空");
            return result;
        }

        Users logon  = usersService.getByObj(users);
        if(logon == null){
            result.setMessages("当前人员未注册");
            return result;
        }
        result.setSuccess(true);
        result.attach("data",logon);
        return result;
    }

    @Override
    public ActionResult getLoginUserInfo(Integer uid) {
        ActionResult result = new ActionResult();
        UserDetail userDetail = usersService.getLoginUserInfo(uid);
//        Users cfg = new Users();
//        cfg.setId(uid);
//        cfg.setEnable(BaseEnum.EnableEnum.Enable.getKey());
//        Users tar = usersService.getByObj(cfg);
//
//        result.setSuccess(tar == null ? false : true);
        return result;
    }
}
