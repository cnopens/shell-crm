package com.crazy.shell.api.users;

import com.crazy.shell.api.base.BaseProvider;
import com.crazy.shell.common.persistent.dao.provide.base.BaseDeleteProvider;
import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.dao.entity.Users;
import com.sun.corba.se.spi.orbutil.fsm.Action;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
public interface UsersProvider extends BaseProvider<Users> {

    /**
     * 人员注册
     * @param company - 企业名称
     * @param users - 用户名/密码
     * @return
     */
    public ActionResult register(Company company, Users users);

    /**
     * 人员登录
     * @param users
     * @return
     */
    public ActionResult getByObj(Users users);


    /**
     * 获取当前登录的休息
     * @param uid : userId
     * @return
     */
    public ActionResult getLoginUserInfo(Integer uid);


}
