package com.crazy.shell.api.guest;

import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.entity.Guest;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 项目名称：shell-crm
 * 创建人: crazy.huang
 * 创建时间：2016/3/2.
 */

public interface GuestProvider {


    /**
     * 根据对象查找
     * @param config
     * @return
     */
    List<Guest> findByObj(Guest config);

    /**
     * 新增客户
     * @param guest
     * @return
     */
    public ActionResult add(Guest guest);

    /**
     * 更新用户
     * @param guest
     * @return
     */
    public ActionResult update(Guest guest);

    public Guest selectById(Integer id);
}
