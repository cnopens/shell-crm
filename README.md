#基本框架
spring + mybatis + velocity
#数据库线程池
druid
##数据通信dubbo
1.下载jar包

    <!--dubbo-->
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>dubbo</artifactId>
        <version>${dubbo.version}</version>
    </dependency>
 
###dubbo的配置

2.1server端配置


    <dubbo:application name="shell-crm-app"></dubbo:application>
    <dubbo:registry id="dubbo-provider-registry"
                    address="zookeeper://172.16.190.198:2181?backup=172.16.190.198:2182,172.16.190.198:2183" check="false"
                    timeout="15000"></dubbo:registry>
    <dubbo:annotation package="com.crazy.shell.server" />

2.1.1server端class配置

    import com.alibaba.dubbo.config.annotation.Service;
    @Service
    public class GuestProviderImpl implements GuestProvider {
        private static final Logger logger = LoggerFactory.getLogger(GuestProviderImpl.class);
    }


2.2client端配置

       <dubbo:annotation />
       <dubbo:application name="shell-crm-app"></dubbo:application>

       <dubbo:registry id="dubbo-consumer-registry"
                       address="zookeeper://172.16.190.198:2181?backup=172.16.190.198:2182,172.16.190.198:2183" check="false"
                       timeout="15000"></dubbo:registry>
       <dubbo:protocol name="dubbo" port="20880"></dubbo:protocol>

2.2.1client 端class配置

    @Controller
    @RequestMapping("/guest")
    public class GuestController {
        @Reference
        private GuestProvider guestProvider;
        @Reference
        private UserProvider userProvider;
    }

2.2.2 springMVC 配置

        <!-- 开启注解功能 -->
       <context:annotation-config />

       <!-- 扫描包文件 -->
       <dubbo:annotation package="com.crazy.shell.website.controller" />
       <context:component-scan base-package="com.crazy.shell.website.controller" />

**dubbo 的扫描必须在springMVC 之前**

##common--shell

3.common中说明

###3.3.1 动态数据源切换

###3.3.2 读写分离切换

###3.3.3 jqGrid的实现

###3.3.4 分页器

### 3.3.5 velocity
