package com.crazy.shell.service.comService;

import com.crazy.shell.common.persistent.service.BaseService;
import com.crazy.shell.dao.entity.ComService;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
public interface ComSerService extends BaseService<ComService>{
}
