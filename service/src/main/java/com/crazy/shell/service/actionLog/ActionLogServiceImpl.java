package com.crazy.shell.service.actionLog;

import com.alibaba.dubbo.config.annotation.Service;
import com.crazy.shell.dao.entity.repository.ActionLog;
import com.crazy.shell.dao.repository.ActionLogRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/21.
 */
@Component
public class ActionLogServiceImpl  implements ActionLogService{
    @Resource
    private ActionLogRepository logRepo;

    public ActionLog save(ActionLog log){
        return  logRepo.save(log);
    }
}
